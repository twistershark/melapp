import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Center(
            child: Text(
              'Mel de Deus',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                  fontFamily: 'FireSans'),
            ),
          ),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(height: 20.0),
                _menuItem('images/prayer.png', 'Orações'),
                _menuItem('images/devocoes.png', 'Devoções'),
                _menuItem('images/question.png', 'Quiz Biblíco'),
              ],
            ),
            Column(
              children: <Widget>[
                SizedBox(height: 20.0),
                _menuItem('images/lectio.png', 'Lectio Divina'),
                _menuItem('images/agenda.png', 'Diário Espiritual'),
                _menuItem('images/heart.png', 'Quero Ajudar'),
              ],
            )
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.amber[700],
          unselectedItemColor: Colors.grey,
          showUnselectedLabels: true,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.description),
              title: Text('Youtube'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.event),
              title: Text('Agenda'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Menu'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              title: Text('Doações'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.perm_phone_msg),
              title: Text('Contato'),
            ),
          ],
        ),
      ),
    );
  }
}

Widget _menuItem(String imgPath, String nome) {
  return Container(
    decoration: BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: Colors.grey,
          blurRadius: 10.0, // soften the shadow
          spreadRadius: 0.05, //extend the shadow
          offset: Offset(3.0, 3.0),
        )
      ],
      color: Colors.white,
      borderRadius: BorderRadius.all(
        Radius.circular(10.0),
      ),
    ),
    height: 150.0,
    width: 155.0,
    margin: EdgeInsets.all(20.0),
    child: Center(
      child: Column(
        children: <Widget>[
          SizedBox(height: 30.0),
          Image(
            image: AssetImage(imgPath),
          ),
          SizedBox(height: 5.0),
          Text(
            nome,
            style: TextStyle(
              color: Colors.amber[700],
              fontFamily: 'FireSans',
              fontWeight: FontWeight.normal,
              fontSize: 18.0,
              shadows: <Shadow>[
                Shadow(
                  offset: Offset(0.0, 0.0),
                  blurRadius: 1.0,
                  color: Color.fromARGB(255, 0, 0, 0),
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}
